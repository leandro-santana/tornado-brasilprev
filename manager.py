import logging
import sys
import sqlalchemy
from pandas import read_csv
from brasilprev import config
from brasilprev.models.model_base import ModelBase

orm = ModelBase()

logger = logging.getLogger(__name__)


def load_user_admin():
    from brasilprev.models.user import User
    try:
        logger.info('---- Start Load Data ----')
        user = User()
        df = read_csv('files/admin_user.csv', sep=',', index_col=None,
                      usecols=['full_name', 'login', 'password'])
        df.to_sql(
            name=user.get_table_name, con=user.orm.db_engine, if_exists='append',
            schema=user.get_schema_name, method='multi', index=False)
        user.orm.remove_session()
        logger.info('---- Finish Load Data ----')
    except Exception as err:
        logger.error(str(err))


def load_customer_user():
    from brasilprev.models.customer import Customer
    try:
        logger.info('---- Start Load Data ----')
        user = Customer()
        df = read_csv('files/customer.csv', sep=',', index_col=None,
                      usecols=['full_name', 'login', 'password'])
        df.to_sql(
            name=user.get_table_name, con=user.orm.db_engine, if_exists='append',
            schema=user.get_schema_name, method='multi', index=False)
        user.orm.remove_session()
        logger.info('---- Finish Load Data ----')
    except Exception as err:
        logger.error(str(err))


def load_purchase_status():
    from brasilprev.models.purchase import PurchaseStatus
    try:
        logger.info('---- Start Load Data ----')
        purchase = PurchaseStatus()
        df = read_csv('files/purchase_status.csv', sep=',', index_col=None,
                      usecols=['name'])
        df.to_sql(
            name=purchase.get_table_name, con=purchase.orm.db_engine, if_exists='append',
            schema=purchase.get_schema_name, method='multi', index=False)
        purchase.orm.remove_session()
        logger.info('---- Finish Load Data ----')
    except Exception as err:
        logger.error(str(err))


def load_products():
    from brasilprev.models.product import Product
    try:
        logger.info('---- Start Load Data ----')
        purchase = Product()
        df = read_csv('files/products.csv', sep=',', index_col=None,
                      usecols=['name', 'value', 'quantity'])
        df.to_sql(
            name=purchase.get_table_name, con=purchase.orm.db_engine, if_exists='append',
            schema=purchase.get_schema_name, method='multi', index=False)

        purchase.orm.remove_session()
        logger.info('---- Finish Load Data ----')

    except Exception as err:
        logger.error(str(err))


def migrate_database():
    """Migrate Database."""
    import brasilprev.models.customer
    import brasilprev.models.product
    import brasilprev.models.user
    import brasilprev.models.purchase
    try:
        logger.info('---- Create Database Structure ----')
        conn = ModelBase().orm.db_engine.connect()

        if not conn.dialect.has_schema(conn, config.SQL_SCHEMA):
            ModelBase().orm.db_engine.execute(sqlalchemy.schema.CreateSchema(config.SQL_SCHEMA))
        ModelBase.metadata.create_all(bind=orm.orm.db_engine, checkfirst=True)
    except Exception as err:
        logger.error(str(err))


if __name__ == '__main__':
    args = sys.argv[1:]

    if 'migrate' in list(args):
        migrate_database()

    if 'content' in list(args):
        load_user_admin()
        load_customer_user()
        load_purchase_status()
        load_products()
