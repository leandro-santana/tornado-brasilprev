import tornado.web

from brasilprev.views.healthcheck import HealthcheckApi
from brasilprev.views.v1.customer import CustomerAdd, CustomerList
from brasilprev.views.v1.product import ProductAdd, ProductList
from brasilprev.views.v1.purchase_item import PurchaseItemAdd, PurchaseItemList, PurchaseItemEdit

routes = [
    tornado.web.url(r'/api/healthcheck', HealthcheckApi),
    tornado.web.url(r'/api/{version}/customer'.format(version=CustomerAdd.__version__), CustomerAdd),
    tornado.web.url(r'/api/{version}/customer-list'.format(version=CustomerList.__version__), CustomerList),
    tornado.web.url(r'/api/{version}/product'.format(version=ProductAdd.__version__), ProductAdd),
    tornado.web.url(r'/api/{version}/product-list'.format(version=ProductList.__version__), ProductList),
    tornado.web.url(r'/api/{version}/purchase-item'.format(version=PurchaseItemAdd.__version__), PurchaseItemAdd),
    tornado.web.url(r'/api/{version}/purchase-item/(?P<purchase_id>[a-zA-Z0-9]+)'.format(
        version=PurchaseItemEdit.__version__), PurchaseItemEdit),
    tornado.web.url(r'/api/{version}/purchase-list'.format(version=PurchaseItemList.__version__), PurchaseItemList)
]
