from http import HTTPStatus
import logging

logger = logging.getLogger(__name__)


class GenericError(Exception):
    code = 500
    message = 'Internal Server Error'

    def __init__(self, message=None):
        if message:
            self.message = message


class CustomDatabaseError(GenericError):
    def __init__(self, message, code=HTTPStatus.BAD_REQUEST.value):
        super(CustomDatabaseError, self).__init__(message)
        self.code = code
        self.message = message


KNOWN_ERRORS = (CustomDatabaseError,)


def default_exception_error(model, error):
    logger.error(str(error))
    model.orm.remove_session()
    raise CustomDatabaseError(message=model.KNOWN_ERROR_SQLALCHEMY.get('friendly_message'))
