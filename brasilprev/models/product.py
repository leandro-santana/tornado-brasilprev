# coding: utf-8
from http import HTTPStatus

from sqlalchemy import Column, Integer, String, Numeric
from tornado.httpclient import HTTPError

from brasilprev import config
from brasilprev.models.model_base import ModelBase


class Product(ModelBase):
    __tablename__ = 'product'
    __table_args__ = {'schema': config.SQL_SCHEMA}

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False, unique=True)
    value = Column(Numeric, nullable=False)
    quantity = Column(Integer, nullable=False, default=0)

    @property
    def get_table_name(self):
        return self.__tablename__

    @property
    def get_schema_name(self):
        return self.__table_args__.get('schema', '')

    def get_product(self, product_id):
        product = self.orm.db_session.query(Product).filter_by(id=product_id)
        if not product:
            raise HTTPError(code=HTTPStatus.NOT_FOUND,
                            message='The product with code {} not found'.format(product_id))
        return product
