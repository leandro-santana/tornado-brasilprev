# coding: utf-8
from sqlalchemy import Column, Integer, String

from brasilprev import config
from brasilprev.models.model_base import ModelBase


class Customer(ModelBase):
    __tablename__ = 'customer'
    __table_args__ = {'schema': config.SQL_SCHEMA}

    id = Column(Integer, primary_key=True)
    full_name = Column(String(255), nullable=False)
    login = Column(String(255), nullable=False, unique=True)
    password = Column(String(255), nullable=False)

    @property
    def get_table_name(self):
        return self.__tablename__

    @property
    def get_schema_name(self):
        return self.__table_args__.get('schema', '')
