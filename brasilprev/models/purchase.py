# coding: utf-8
import logging
from http import HTTPStatus

from sqlalchemy import Column, Integer, Numeric, ForeignKey, String, func
from sqlalchemy.orm import relation
from tornado.httpclient import HTTPError

from brasilprev import config
from brasilprev.models.customer import Customer
from brasilprev.models.model_base import ModelBase
from brasilprev.models.product import Product

logger = logging.getLogger(__name__)


class PurchaseStatus(ModelBase):
    __tablename__ = 'purchase_status'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False, unique=True)

    __table_args__ = {'schema': config.SQL_SCHEMA}

    @property
    def get_table_name(self):
        return self.__tablename__

    @property
    def get_schema_name(self):
        return self.__table_args__.get('schema', '')

    def get_purchase_status(self, name):
        return self.orm.db_session.query(PurchaseStatus).filter(
            PurchaseStatus.name == name).first()


class Purchase(ModelBase):
    __tablename__ = 'purchase'

    id = Column(Integer, primary_key=True)
    total_value = Column(Numeric)
    customer_id = Column(Integer, ForeignKey(Customer.id))
    customer = relation(Customer)
    purchase_status_id = Column(Integer, ForeignKey(PurchaseStatus.id))
    purchase_status = relation(PurchaseStatus)

    __table_args__ = {'schema': config.SQL_SCHEMA}

    @property
    def get_table_name(self):
        return self.__tablename__

    @property
    def get_schema_name(self):
        return self.__table_args__.get('schema', '')

    def get_or_create_purchase_by_consumer(self, customer_id):

        model_purchase_status_model = PurchaseStatus()

        if not customer_id:
            logger.error('customer id not found.')
            raise HTTPError(code=HTTPStatus.BAD_REQUEST.value, message='customer_id error')

        purchase_status = model_purchase_status_model.get_purchase_status(name=config.PENDING_STATUS_NAME.lower())

        purchase = self.orm.db_session.query(Purchase).filter(
            Purchase.purchase_status_id == purchase_status.id,
            Purchase.customer_id == customer_id
        ).first()

        if not purchase:
            purchase = Purchase(
                customer_id=customer_id,
                purchase_status_id=purchase_status.id,
                total_value=0
            )
            self.orm.object_commit(purchase)
        return purchase

    def update_purchase_total_value(self, purchase_id, total_purchase):

        if not total_purchase:
            logger.error('Total Not Found')
            raise HTTPError(code=HTTPStatus.NOT_FOUND, message='Purchase ID e Total Not Found')

        purchase = self.orm.db_session.query(Purchase).filter_by(id=purchase_id)
        purchase.update({'total_value': total_purchase})
        self.orm.object_commit(purchase.first())


class PurchaseItem(ModelBase):
    __tablename__ = 'purchase_item'

    id = Column(Integer, primary_key=True)
    purchase_id = Column(Integer, ForeignKey(Purchase.id))
    product_id = Column(Integer, ForeignKey(Product.id))
    quantity = Column(Integer)
    total_item_value = Column(Numeric)

    purchase = relation(Purchase)
    product = relation(Product)

    __table_args__ = {'schema': config.SQL_SCHEMA}

    @property
    def get_table_name(self):
        return self.__tablename__

    @property
    def get_schema_name(self):
        return self.__table_args__.get('schema', '')

    def get_product_open(self, product_id, purchase_id):
        return self.orm.db_session.query(PurchaseItem.id.label('purchase_item_id'),
                                         Product.id.label('product_id'),
                                         PurchaseStatus.name.label('status')).join(
            PurchaseItem, Purchase, PurchaseStatus).filter(
            Product.id == product_id, Purchase.id == purchase_id,
            PurchaseStatus.name == config.PENDING_STATUS_NAME.lower()).first()

    def get_purchase_item(self, purchase_item_id):
        purchase = self.orm.db_session.query(PurchaseItem).filter_by(id=purchase_item_id)
        if not purchase:
            logger.error('purchase not found.')
            raise HTTPError(code=HTTPStatus.BAD_REQUEST.value, message='purchase not found.')
        return purchase

    def create_or_update_purchase_item(self, product_id, purchase_id, quantity, product_value):
        purchase_item_selected = None
        product_open = self.get_product_open(product_id=product_id, purchase_id=purchase_id)

        if product_open:
            purchase_item = self.get_purchase_item(
                purchase_item_id=product_open.purchase_item_id)

            purchase_item_selected = purchase_item.first()

            purchase_item.update({
                'quantity': purchase_item_selected.quantity + quantity,
                'total_item_value': purchase_item_selected.total_item_value + (product_value * int(quantity))}
            )
            self.orm.object_commit(purchase_item_selected)

        if not purchase_item_selected:
            purchase_item_selected = PurchaseItem(
                purchase_id=purchase_id,
                product_id=product_id,
                quantity=quantity,
                total_item_value=product_value * int(quantity)
            )

        self.orm.object_commit(purchase_item_selected)
        return purchase_item_selected

    def get_purchase_item_total(self, purchase_id):
        total = self.orm.db_session.query(
            func.sum(PurchaseItem.total_item_value).label("total_purchase")).filter(
            PurchaseItem.purchase_id == purchase_id).group_by(PurchaseItem.purchase_id).first()
        return total.total_purchase
