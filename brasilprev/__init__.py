from tornado.web import Application
from tornado_swagger.setup import setup_swagger

from brasilprev.configs import config
from brasilprev.urls import routes


class ApiApplication(Application):

    def __init__(self):
        settings = {
            'debug': config.APP_DEBUG
        }

        setup_swagger(routes,
                      swagger_url='/doc',
                      description='Api to available knowledge with python',
                      api_version=config.APP_VERSION,
                      title=config.APP_TITLE)

        super(ApiApplication, self).__init__(routes, **settings)
