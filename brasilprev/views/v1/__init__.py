from tornado_swagger.components import components


@components.parameters.register
class UserParameters:
    """
    ---
    - name: Username
    in: header
    description: system username
    required: true
    schema:
      type: string
    - name: Password
    in: header
    description: system password
    required: true
    schema:
      type: string
    """


@components.schemas.register
class ArrayResponseOfModel:
    """
    ---
    type: object
    description: Response Representation
    properties:
        message:
            type: object
        status:
            type: string
    """


@components.schemas.register
class ModelResponseError:
    """
    ---
    type: object
    description: Response Error Representation
    properties:
        message:
            type: string
        status:
            type: string
    """


@components.schemas.register
class CustomerModel(object):
    """
    ---
    type: object
    description: Customer model representation
    properties:
        full_name:
            type: string
        login:
            type: string
        password:
            type: string
    """


@components.schemas.register
class ProductModel(object):
    """
    ---
    type: object
    description: Product model representation
    properties:
        name:
            type: string
        value:
            type: number
        quantity:
            type: integer
    """


@components.schemas.register
class PurchaseItemModel(object):
    """
    ---
    type: object
    description: Purchase Item Model representation
    properties:
        product_id:
            type: integer
        quantity:
            type: integer
    """
