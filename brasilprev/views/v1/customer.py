from http import HTTPStatus

from tornado.httpclient import HTTPError

from brasilprev.exceptions import KNOWN_ERRORS
from brasilprev.helpers.base_factory import BaseDefaultFactory
from brasilprev.views import ApiJsonHandler


class CustomerAdd(ApiJsonHandler):
    __version__ = 'v1'

    async def post(self):
        """
        ---
        tags:
          - Customer
        description: Post to Customer Register
        operationId: PostCustomer
        parameters:
          - name: Username
            in: header
            description: system username
            required: true
            schema:
              type: string
          - name: Password
            in: header
            description: system password
            required: true
            schema:
              type: string
        requestBody:
          description: payload to customer register
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CustomerModel'
          required: true
        responses:
          '201':
            description: create customer
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ArrayResponseOfModel'
          'errors':
            description: response errors
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ModelResponseError'
        """
        try:
            instance = BaseDefaultFactory.get_instance(carrier='customer')
            params = await instance.agreement(
                request=self.request, full_name=True, login=True, password=True, location='json')

            response = await instance.process(params=params, method=self.request.method)
            self.success(code=HTTPStatus.CREATED.value, message=response)
        except HTTPError as error:
            self.error(code=error.code, message=error.message)
        except KNOWN_ERRORS as error:
            self.error(code=error.code, message=error.message)


class CustomerList(ApiJsonHandler):
    __version__ = 'v1'

    async def get(self):
        """
        ---
        tags:
          - Customer
        summary: Get Customer
        description: 'Get Customer'
        operationId: getCustomer
        parameters:
          - name: target
            in: query
            description: target field
            required: true
            schema:
              enum: [id, name]
              type: string
          - name: value
            in: query
            description: value to search in target field
            required: true
            schema:
              type: string
          - name: page
            in: query
            description: page to initial select
            required: true
            schema:
              type: string
          - name: page_size
            in: query
            description: quantity of items
            required: true
            schema:
              type: string
          - name: Username
            in: header
            description: admin username
            required: true
            schema:
              type: string
          - name: Password
            in: header
            description: admin password
            required: true
            schema:
              type: string
        responses:
            '200':
              description: list Customer
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/ArrayResponseOfModel'
            'errors':
              description: response errors
              content:
               application/json:
                schema:
                  $ref: '#/components/schemas/ModelResponseError'
        """
        try:
            instance = BaseDefaultFactory.get_instance(carrier='customer_detail')
            params = await instance.agreement(request=self.request)

            response = await instance.process(params=params)
            self.success(code=HTTPStatus.OK.value, message=response)
        except HTTPError as error:
            self.error(code=error.code, message=error.message)
        except KNOWN_ERRORS as error:
            self.error(code=error.code, message=error.message)
