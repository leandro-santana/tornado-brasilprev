from http import HTTPStatus

from tornado.httpclient import HTTPError

from brasilprev.exceptions import KNOWN_ERRORS
from brasilprev.helpers.base_factory import BaseDefaultFactory
from brasilprev.views import ApiJsonHandler


class PurchaseItemAdd(ApiJsonHandler):
    __version__ = 'v1'

    async def post(self):
        """
        ---
        tags:
          - Purchase
        description: Post to Purchase Item
        operationId: PostPurchase
        parameters:
          - name: Username
            in: header
            description: costumer username
            required: true
            schema:
              type: string
          - name: Password
            in: header
            description: costumer password
            required: true
            schema:
              type: string
        requestBody:
          description: payload to purchase item include
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PurchaseItemModel'
          required: true
        responses:
          '201':
            description: create purchase item
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ArrayResponseOfModel'
          'errors':
            description: response errors
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/ModelResponseError'
        """
        try:
            instance = BaseDefaultFactory.get_instance(carrier='purchase_item')
            params = await instance.agreement(request=self.request, location='json')

            response = await instance.process(params=params, method=self.request.method)
            self.success(code=HTTPStatus.CREATED.value, message=response)
        except HTTPError as error:
            self.error(code=error.code, message=error.message)
        except KNOWN_ERRORS as error:
            self.error(code=error.code, message=error.message)


class PurchaseItemList(ApiJsonHandler):
    __version__ = 'v1'

    async def get(self):
        """
        ---
        tags:
          - Purchase
        summary: Get Purchase and PurchaseItems
        description: 'Get purchase'
        operationId: getPurchase
        parameters:
          - name: target
            in: query
            description: target field
            required: true
            schema:
              enum: [purchase_id, purchase_status]
              type: string
          - name: value
            in: query
            description: value to search in target field
            required: true
            schema:
              type: string
          - name: page
            in: query
            description: page to initial select
            required: true
            schema:
              type: string
          - name: page_size
            in: query
            description: quantity of items
            required: true
            schema:
              type: string
          - name: Username
            in: header
            description: costumer username
            required: true
            schema:
              type: string
          - name: Password
            in: header
            description: costumer password
            required: true
            schema:
              type: string
        responses:
            '200':
              description: list of purchase and purchase item
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/ArrayResponseOfModel'
            'errors':
              description: response errors
              content:
               application/json:
                schema:
                  $ref: '#/components/schemas/ModelResponseError'
        """
        try:
            instance = BaseDefaultFactory.get_instance(carrier='purchase_detail')
            params = await instance.agreement(request=self.request)

            response = await instance.process(params=params)
            self.success(code=HTTPStatus.OK.value, message=response)
        except HTTPError as error:
            self.error(code=error.code, message=error.message)
        except KNOWN_ERRORS as error:
            self.error(code=error.code, message=error.message)


class PurchaseItemEdit(ApiJsonHandler):
    __version__ = 'v1'

    async def put(self, purchase_id):
        """
        ---
        tags:
          - Purchase
        summary: Conclude Purchase
        description: Conclude
        operationId: UpdatePurchase
        parameters:
          - name: purchase_id
            in: path
            description: id to conclude purchase
            required: true
            schema:
              type: integer
              format: int64
          - name: Username
            in: header
            description: costumer username
            required: true
            schema:
              type: string
          - name: Password
            in: header
            description: costumer password
            required: true
            schema:
              type: string
        responses:
            '200':
              description: update specific of weather
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/ArrayResponseOfModel'
            'errors':
              description: response errors
              content:
               application/json:
                schema:
                  $ref: '#/components/schemas/ModelResponseError'
        """
        try:
            instance = BaseDefaultFactory.get_instance(carrier='purchase_item')
            params = await instance.agreement(request=self.request, product_id=False, quantity=False)

            params.update({'id': purchase_id})
            response = await instance.process(params=params, method=self.request.method)

            self.success(code=HTTPStatus.OK.value, message=response)
        except HTTPError as error:
            self.error(code=error.code, message=error.message)
        except KNOWN_ERRORS as error:
            self.error(code=error.code, message=error.message)
