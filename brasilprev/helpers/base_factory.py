import logging
from http import HTTPStatus
from tornado.httpclient import HTTPError

from brasilprev.controller.consumer_detail import CustomerDetail
from brasilprev.controller.customer import CustomerController
from brasilprev.controller.product import ProductController
from brasilprev.controller.product_detail import ProductDetail
from brasilprev.controller.purchase_detail import PurchaseDetail
from brasilprev.controller.purchase_item import PurchaseItemController
from brasilprev.helpers import ServiceBase

__all__ = ['BaseDefaultFactory', 'CustomerController', 'ProductController',
           'PurchaseItemController', 'PurchaseDetail', 'ProductDetail', 'CustomerDetail']

LIST_ABSTRACTS = [ServiceBase]
logger = logging.getLogger(__name__)


class BaseDefaultFactory:
    @staticmethod
    def get_instance(carrier):

        for abstracts_classes in LIST_ABSTRACTS:
            for klass in abstracts_classes.__subclasses__():
                if carrier.lower() == klass.carrier:
                    return klass()
        message = 'instance not found {}'.format(carrier)
        logger.exception(message)
        raise HTTPError(code=HTTPStatus.NOT_FOUND.value, message=message)
