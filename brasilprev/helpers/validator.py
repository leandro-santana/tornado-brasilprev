import hashlib
from http import HTTPStatus
from brasilprev import config

from tornado.httpclient import HTTPError
from webargs import ValidationError
from brasilprev.models.customer import Customer
from brasilprev.models.user import User


class CustomerLoginValidate(object):
    def __init__(self, func):
        self.func = func

    async def __call__(self, *args, **kwargs):
        validate = await self.__validate_customer_user(**kwargs)

        if not validate:
            raise HTTPError(message='User is not authorized', code=HTTPStatus.UNAUTHORIZED.value)

        kwargs.update({'customer_id': validate.id})
        return await self.func(**kwargs)

    async def __validate_customer_user(self, **kwargs):
        query = Customer().orm.db_session.query(Customer)

        customer = query.filter(
            Customer.login == kwargs.get('request').headers.get('Username', ''),
            Customer.password == hashlib.sha224(
                str(kwargs.get('request').headers.get('Password', '')).encode()).hexdigest()
        ).first()
        return customer


class UserAdminLoginValidate(object):

    def __init__(self, func):
        self.func = func

    async def __call__(self, *args, **kwargs):
        validate = await self.__validate_user_admin(**kwargs)

        if not validate:
            raise HTTPError(message='User is not authorized', code=HTTPStatus.UNAUTHORIZED.value)

        return await self.func(**kwargs)

    async def __validate_user_admin(self, **kwargs):
        query = User().orm.db_session.query(User)

        user = query.filter(
            User.login == kwargs.get('request').headers.get('Username', ''),
            User.password == hashlib.sha224(str(kwargs.get('request').headers.get('Password', '')).encode()).hexdigest()
        ).first()
        return user


def validate_default_with_min_value(value):
    if value in ['string', False, None]:
        raise ValidationError("Param Invalid")

    if len(value) < int(config.MINVALUEVALIDATE):
        raise ValidationError("have to be greater than {}".format(config.MINVALUEVALIDATE))
