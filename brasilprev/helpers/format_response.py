def purchase_response(response):
    return {
        'purchase_id': response.purchase.id,
        'purchase_total_value': str(response.purchase.total_value),
        'customer': response.purchase.customer.full_name,
        'product': response.product.name,
        'quantity': response.quantity,
        'total_item_value': str(response.total_item_value),
        'purchase_data': str(response.purchase.time_created)
    }


def product_response(response):
    return {
        'id': response.id,
        'name': response.name,
        'value': str(response.value),
        'quantity': response.quantity
    }


def customer_response(response):
    return {
        'id': response.id,
        'name': response.full_name,
        'login': response.login
    }
