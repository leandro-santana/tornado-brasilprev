import base64
from abc import ABCMeta, abstractmethod


def generate_hash(value):
    value = value.encode("ascii")
    return base64.b64encode(value).decode('utf8')


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    @staticmethod
    def drop():
        """Drop the instance (for testing purposes)"""
        Singleton._instances = {}


class SingletonComplexity(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        hash_cls = generate_hash(str(args) + str(kwargs))
        if hash_cls not in cls._instances:
            cls._instances[hash_cls] = super(SingletonComplexity, cls).__call__(*args, **kwargs)
        return cls._instances[hash_cls]

    @staticmethod
    def drop():
        """Drop the instance (for testing purposes)"""
        SingletonComplexity._instances = {}


class ServiceHTTPCommon(metaclass=ABCMeta):

    @abstractmethod
    def method_post(self, **kwargs):
        raise NotImplementedError("Implement me")


class ServiceBaseDetail(metaclass=ABCMeta):
    @abstractmethod
    def query_mount(self, target, value, page, size, **kwargs):
        raise NotImplementedError("Implement me")

    @abstractmethod
    def result_mount(self, obj):
        raise NotImplementedError("Implement me")


class ServiceBase(metaclass=ABCMeta):

    @abstractmethod
    def process(self, params, **kwargs):
        raise NotImplementedError("Implement me")

    @abstractmethod
    def agreement(self, request):
        raise NotImplementedError("Implement me")
