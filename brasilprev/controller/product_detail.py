import logging

from sqlalchemy import func
from tornado.httpclient import HTTPError
from http import HTTPStatus

from brasilprev.controller import MixinDetail, default_list_agreement_agreement
from brasilprev.exceptions import default_exception_error
from brasilprev.helpers import ServiceBase
from brasilprev.helpers.format_response import product_response
from brasilprev.helpers.paginator import paginate
from brasilprev.helpers.validator import UserAdminLoginValidate
from brasilprev.models.product import Product

logger = logging.getLogger(__name__)


class ProductDetail(MixinDetail, ServiceBase):
    carrier = 'product_detail'

    @staticmethod
    @UserAdminLoginValidate
    async def agreement(**kwargs):
        return default_list_agreement_agreement(**kwargs)

    async def query_mount(self, target, value, page, size, **kwargs):
        """ query mount """

        query = Product().orm.db_session.query(Product)

        condition = {
            "id": query.filter_by(id=value),
            "name": query.filter(func.lower(Product.name).like("%{}%".format(value))),
        }

        if target not in condition:
            message = 'target "{}" not found in model Product'.format(target)
            logger.error(message)
            raise HTTPError(code=HTTPStatus.BAD_REQUEST.value, message=message)

        result = {}
        try:
            result = paginate(condition[target], page, size)
        except Product().KNOWN_ERROR_SQLALCHEMY.get('known_errors') as error:
            default_exception_error(model=Product(), error=error)

        return result

    async def result_mount(self, obj):
        """ result mount """
        return product_response(obj)

    async def process(self, params, **kwargs):
        response = await self._run_process(params, **kwargs)
        Product().orm.remove_session()
        return response
