import logging

from sqlalchemy import func
from tornado.httpclient import HTTPError
from http import HTTPStatus

from brasilprev.controller import MixinDetail, default_list_agreement_agreement
from brasilprev.exceptions import default_exception_error
from brasilprev.helpers import ServiceBase
from brasilprev.helpers.format_response import customer_response
from brasilprev.helpers.paginator import paginate
from brasilprev.helpers.validator import UserAdminLoginValidate
from brasilprev.models.customer import Customer

logger = logging.getLogger(__name__)


class CustomerDetail(MixinDetail, ServiceBase):
    carrier = 'customer_detail'

    @staticmethod
    @UserAdminLoginValidate
    async def agreement(**kwargs):
        return default_list_agreement_agreement(**kwargs)

    async def query_mount(self, target, value, page, size, **kwargs):
        """ query mount """

        query = Customer().orm.db_session.query(Customer)

        condition = {
            "id": query.filter_by(id=value),
            "name": query.filter(func.lower(Customer.full_name).like("%{}%".format(value))),
        }

        if target not in condition:
            message = 'target "{}" not found in model Customer'.format(target)
            logger.error(message)
            raise HTTPError(code=HTTPStatus.BAD_REQUEST.value, message=message)

        result = {}
        try:
            result = paginate(condition[target], page, size)
        except Customer().KNOWN_ERROR_SQLALCHEMY.get('known_errors') as error:
            default_exception_error(model=Customer(), error=error)

        return result

    async def result_mount(self, obj):
        """ result mount """
        return customer_response(obj)

    async def process(self, params, **kwargs):
        response = await self._run_process(params, **kwargs)
        Customer().orm.remove_session()
        return response
