import logging
from http import HTTPStatus

from tornado.httpclient import HTTPError
from webargs import fields
from webargs.tornadoparser import parser

from brasilprev import config
from brasilprev.controller import MixinBase
from brasilprev.exceptions import default_exception_error
from brasilprev.helpers import ServiceBase
from brasilprev.helpers.validator import CustomerLoginValidate
from brasilprev.models.product import Product
from brasilprev.models.purchase import PurchaseItem, Purchase, PurchaseStatus

logger = logging.getLogger(__name__)


class PurchaseItemController(MixinBase, ServiceBase):
    carrier = 'purchase_item'

    def __init__(self):
        self.purchase_item_model = PurchaseItem
        self.purchase_model = Purchase
        self.purchase_status_model = PurchaseStatus
        self.product_model = Product

    @staticmethod
    @CustomerLoginValidate
    async def agreement(**kwargs):
        agreement = {
            "product_id": fields.Integer(required=kwargs.get('product_id', True), validate=lambda val: val > 0),
            "quantity": fields.Integer(required=kwargs.get('quantity', True), validate=lambda val: val > 0)
        }

        params = parser.parse(agreement, kwargs.get('request'), location=(kwargs.get('location', 'json')))
        params.update({'customer_id': kwargs.get('customer_id')})
        return params

    async def _get_object(self, **kwargs):

        purchase_status = self.purchase_status_model().get_purchase_status(name=config.PENDING_STATUS_NAME.lower())
        purchase = self.purchase_model().orm.db_session.query(Purchase).filter(
            Purchase.purchase_status_id == purchase_status.id,
            Purchase.id == int(kwargs.get('id'))
        )

        if kwargs.get('customer_id'):
            purchase = purchase.filter(Purchase.customer_id == kwargs.get('customer_id'))

        return purchase

    async def method_put(self, **kwargs):
        """ Update Purchase To conclude"""

        purchase = await self._get_object(**kwargs)
        purchase_content = purchase.first()
        if not purchase_content:
            raise HTTPError(code=HTTPStatus.NOT_FOUND,
                            message='The purchase with code {} not found'.format(kwargs.get('id')))

        try:
            purchase_status = self.purchase_status_model().get_purchase_status(name='concluded')
            purchase.update({'purchase_status_id': purchase_status.id})
        except self.purchase_model.KNOWN_ERROR_SQLALCHEMY.get('known_errors') as error:
            default_exception_error(model=self.purchase_model, error=error)

        self.purchase_model().orm.object_commit(purchase_content)
        self.purchase_model().orm.remove_session()

        return 'Purchase update to concluded successfully.'

    async def method_post(self, **kwargs):
        """
        Get or Create Purchase and PurchaseItems
        """
        try:

            product_id = kwargs.get('product_id')
            quantity = kwargs.get('quantity')
            customer_id = kwargs.get('customer_id')

            purchase = self.purchase_model().get_or_create_purchase_by_consumer(customer_id=customer_id)
            product = self.product_model().get_product(product_id=product_id)

            message = "We Don't have {} to {} more, sorry.".format(quantity, product.first().name)
            if product.first().quantity >= quantity:
                purchase_item = self.purchase_item_model().create_or_update_purchase_item(
                    product_id=product_id, purchase_id=purchase.id,
                    quantity=quantity, product_value=product.first().value)

                Purchase().update_purchase_total_value(
                    purchase_id=purchase.id,
                    total_purchase=self.purchase_item_model().get_purchase_item_total(purchase_id=purchase.id))

                message = 'Purchase: {} updated successfully'.format(purchase_item.purchase_id)
                product.update({'quantity': product.first().quantity - quantity})
                self.product_model().orm.object_commit(product.first())
                self.purchase_item_model().orm.remove_session()
            return message

        except self.purchase_item_model.KNOWN_ERROR_SQLALCHEMY.get('known_errors') as error:
            default_exception_error(model=self.purchase_item_model, error=error)

    async def process(self, params, **kwargs):
        return await self._run_process(params, **kwargs)
