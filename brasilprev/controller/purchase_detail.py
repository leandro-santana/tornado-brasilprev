import logging

from sqlalchemy import func
from tornado.httpclient import HTTPError
from http import HTTPStatus

from brasilprev.controller import MixinDetail
from brasilprev.exceptions import default_exception_error
from brasilprev.helpers import ServiceBase
from brasilprev.helpers.format_response import purchase_response
from brasilprev.helpers.paginator import paginate
from brasilprev.helpers.validator import CustomerLoginValidate
from brasilprev.models.purchase import Purchase, PurchaseItem, PurchaseStatus
from webargs import fields, validate
from webargs.tornadoparser import parser

logger = logging.getLogger(__name__)


class PurchaseDetail(MixinDetail, ServiceBase):
    carrier = 'purchase_detail'

    @staticmethod
    @CustomerLoginValidate
    async def agreement(**kwargs):
        agreement = {
            "target": fields.Str(validate=validate.Length(min=1), required=True),
            "value": fields.Str(validate=validate.Length(min=1), required=True),
            "page": fields.Int(default=0, required=True),
            "page_size": fields.Int(default=10, required=True)}

        params = parser.parse(agreement, kwargs.get('request'), location="query")
        params.update({'customer_id': kwargs.get('customer_id')})
        return params

    async def query_mount(self, target, value, page, size, **kwargs):
        """ query mount """

        query = PurchaseItem().orm.db_session.query(PurchaseItem).join(Purchase, PurchaseStatus).filter(
            Purchase.customer_id == kwargs.get('customer_id'))

        condition = {
            "purchase_id": query.filter(Purchase.id == value),
            "purchase_status": query.filter(func.lower(PurchaseStatus.name).like("%{}%".format(value))),
        }

        if target not in condition:
            message = 'target "{}" not found in model Purchase'.format(target)
            logger.error(message)
            raise HTTPError(code=HTTPStatus.BAD_REQUEST.value, message=message)

        result = {}
        try:
            result = paginate(condition[target], page, size)
        except PurchaseItem().KNOWN_ERROR_SQLALCHEMY.get('known_errors') as error:
            default_exception_error(model=PurchaseItem(), error=error)

        return result

    async def result_mount(self, obj):
        """ result mount """
        return purchase_response(obj)

    async def process(self, params, **kwargs):
        response = await self._run_process(params, **kwargs)
        PurchaseItem().orm.remove_session()
        return response
