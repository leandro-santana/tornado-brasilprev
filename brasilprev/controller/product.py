import logging
from webargs import fields
from webargs.tornadoparser import parser

from brasilprev.controller import MixinBase
from brasilprev.exceptions import default_exception_error
from brasilprev.helpers import ServiceBase
from brasilprev.helpers.validator import UserAdminLoginValidate, validate_default_with_min_value
from brasilprev.models.product import Product

logger = logging.getLogger(__name__)


class ProductController(MixinBase, ServiceBase):
    carrier = 'product'

    def __init__(self):
        self.model = Product()

    @staticmethod
    @UserAdminLoginValidate
    async def agreement(**kwargs):
        agreement = {
            "name": fields.Str(required=kwargs.get('name', False), validate=validate_default_with_min_value),
            "value": fields.Float(required=kwargs.get('value', False), validate=lambda val: val > 0),
            "quantity": fields.Integer(required=kwargs.get('quantity', False), default=0.00)
        }

        params = parser.parse(agreement, kwargs.get('request'), location=(kwargs.get('location', 'json')))
        return params

    async def method_post(self, **kwargs):
        """ Create Products """
        try:
            _post = Product(**kwargs)
            self.model.orm.object_commit(_post)
        except self.model.KNOWN_ERROR_SQLALCHEMY.get('known_errors') as error:
            default_exception_error(model=self.model, error=error)

        return 'object created successfully'

    async def process(self, params, **kwargs):
        return await self._run_process(params, **kwargs)
