import logging
import hashlib
from webargs import fields
from webargs.tornadoparser import parser

from brasilprev.controller import MixinBase
from brasilprev.exceptions import default_exception_error
from brasilprev.helpers import ServiceBase
from brasilprev.helpers.validator import UserAdminLoginValidate, validate_default_with_min_value
from brasilprev.models.customer import Customer

logger = logging.getLogger(__name__)


class CustomerController(MixinBase, ServiceBase):
    carrier = 'customer'

    def __init__(self):
        self.model = Customer()

    @staticmethod
    @UserAdminLoginValidate
    async def agreement(**kwargs):

        agreement = {
            "full_name": fields.Str(required=kwargs.get('full_name', False), validate=validate_default_with_min_value),
            "login": fields.Str(required=kwargs.get('login', False), validate=validate_default_with_min_value),
            "password": fields.Str(required=kwargs.get('password', False), validate=validate_default_with_min_value)
        }

        params = parser.parse(agreement, kwargs.get('request'), location='json')
        return params

    async def method_post(self, **kwargs):
        """ Create Customers """
        try:
            kwargs.update({'password': hashlib.sha224(str(kwargs.get('password', '')).encode()).hexdigest()})
            _post = Customer(**kwargs)
            self.model.orm.object_commit(_post)
        except self.model.KNOWN_ERROR_SQLALCHEMY.get('known_errors') as error:
            default_exception_error(model=self.model, error=error)

        return 'object created successfully'

    async def process(self, params, **kwargs):
        return await self._run_process(params, **kwargs)
