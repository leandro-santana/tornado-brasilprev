from http import HTTPStatus
from tornado.httpclient import HTTPError
from brasilprev.helpers import ServiceHTTPCommon, ServiceBaseDetail
from webargs import fields, validate
from webargs.tornadoparser import parser


def default_list_agreement_agreement(**kwargs):
    agreement = {
        "target": fields.Str(validate=validate.Length(min=1), required=True),
        "value": fields.Str(validate=validate.Length(min=1), required=True),
        "page": fields.Int(default=0, required=True),
        "page_size": fields.Int(default=10, required=True)}

    params = parser.parse(agreement, kwargs.get('request'), location="query")
    return params


class MixinBase(ServiceHTTPCommon):
    async def _run_process(self, params, **kwargs):
        method = kwargs.get('method', '')
        formatter_method = 'method_{}'.format(method.lower())

        if not hasattr(self, formatter_method):
            raise HTTPError(
                code=HTTPStatus.NOT_FOUND.value, message='controller to method {} not found'.format(method))

        found_method = getattr(self, formatter_method)

        return await found_method(**params)


class MixinDetail(ServiceBaseDetail):
    carrier = None

    async def _run_process(self, params, **kwargs):
        target, value = str(params.get('target')).lower(), str(params.get('value')).lower()
        page, size = params.get('page'), params.get('page_size')

        result = await self.query_mount(
            target=target, value=value, page=page, size=size, customer_id=params.get('customer_id'))

        data = {
            'resultsCount': result.total,
            'next_page': result.next_page,
            'previous_page': result.previous_page,
            'data': [await self.result_mount(obj) for obj in result.items]
        }

        return data
