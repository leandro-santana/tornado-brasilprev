import json
from http import HTTPStatus

import mock
from tornado.concurrent import Future
from tornado.httpclient import HTTPError

from brasilprev.exceptions import CustomDatabaseError
from tests.unit.brasilprev import MixinTest, BaseAsyncHttpTestCase


@mock.patch('brasilprev.views.logger', mock.MagicMock())
class TestApiPurchaseList(MixinTest, BaseAsyncHttpTestCase):
    def setUp(self):
        super(TestApiPurchaseList, self).setUp()
        self.url = '/api/v1/purchase-list?target=id&value=1&page=1&page_size=1'

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_get_on_success(self, mock_base_default_factory):
        mock_base_default_factory.get_instance = mock.MagicMock(return_value=self.InstancePurchase())
        response = self.fetch(self.url, raise_error=False, method="GET")
        self.assertEqual(HTTPStatus.OK.value, response.code)
        self.assertIn('result', json.loads(response.body.decode()))

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_get_on_error_http(self, mock_base_default_factory):
        mock_base_default_factory.get_instance().process.side_effect = HTTPError(
            HTTPStatus.BAD_REQUEST.value, 'mock')
        response = self.fetch(self.url, raise_error=False, method="GET")
        self.assertEqual(response.code, HTTPStatus.BAD_REQUEST.value)
        self.assertEqual(response.reason, 'Bad Request')

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_get_on_error_internal_error(self, mock_base_default_factory):
        mock_base_default_factory.get_instance().process.side_effect = Exception('mock error')
        response = self.fetch(self.url, raise_error=False, method="GET")
        self.assertEqual(response.code, HTTPStatus.INTERNAL_SERVER_ERROR.value)

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_get_on_known_error(self, mock_base_default_factory):
        mock_base_default_factory.get_instance().process.side_effect = CustomDatabaseError(message='mock error')
        response = self.fetch(self.url, raise_error=False, method="GET")
        self.assertEqual(response.code, HTTPStatus.BAD_REQUEST.value)


@mock.patch('brasilprev.views.logger', mock.MagicMock())
class TestApiPurchaseAdd(MixinTest, BaseAsyncHttpTestCase):

    def setUp(self):
        super(TestApiPurchaseAdd, self).setUp()
        self.url = '/api/v1/purchase-item'

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_post_on_success(self, mock_base_default_factory):
        post = {
            "product_id": 1,
            "quantity": 10
        }
        message = "object created successfully"
        mock_base_default_factory.get_instance = mock.MagicMock(
            return_value=self.InstanceEdit(mock_result=message))
        response = self.fetch(self.url, raise_error=False, method='POST', body=json.dumps(post))

        self.assertEqual(HTTPStatus.CREATED.value, response.code)
        self.assertEqual(message, json.loads(response.body.decode()).get('result'))

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_post_on_error_http(self, mock_base_default_factory):
        mock_base_default_factory.get_instance().process.side_effect = HTTPError(
            HTTPStatus.BAD_REQUEST.value, 'mock')

        future_1 = Future()
        future_1.set_result({})
        mock_base_default_factory.get_instance().agreement = mock.MagicMock(return_value=future_1)

        response = self.fetch(self.url, raise_error=False, method="POST", body=json.dumps({}))
        self.assertEqual(response.code, HTTPStatus.BAD_REQUEST.value)
        self.assertEqual(response.reason, 'Bad Request')

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_post_on_known_error(self, mock_base_default_factory):
        mock_base_default_factory.get_instance().process.side_effect = CustomDatabaseError(message='mock error')

        future_1 = Future()
        future_1.set_result({})
        mock_base_default_factory.get_instance().agreement = mock.MagicMock(return_value=future_1)

        response = self.fetch(self.url, raise_error=False, method="POST", body=json.dumps({}))
        self.assertEqual(response.code, HTTPStatus.BAD_REQUEST.value)
        self.assertEqual(response.reason, 'Bad Request')


@mock.patch('brasilprev.views.logger', mock.MagicMock())
class TestApiWeatherEdit(MixinTest, BaseAsyncHttpTestCase):

    def setUp(self):
        super(TestApiWeatherEdit, self).setUp()
        self.url = '/api/v1/purchase-item/1'

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_put_on_success(self, mock_base_default_factory):
        mock_base_default_factory.get_instance = mock.MagicMock(return_value=self.InstanceEdit())
        response = self.fetch(self.url, raise_error=False, method='PUT', body=json.dumps({}))

        self.assertEqual(HTTPStatus.OK.value, response.code)
        self.assertIn('result', json.loads(response.body.decode()))

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_put_on_error_http(self, mock_base_default_factory):
        mock_base_default_factory.get_instance().process.side_effect = HTTPError(
            HTTPStatus.BAD_REQUEST.value, 'mock')

        future_1 = Future()
        future_1.set_result({})
        mock_base_default_factory.get_instance().agreement = mock.MagicMock(return_value=future_1)

        response = self.fetch(self.url, raise_error=False, method="PUT", body=json.dumps({}))
        self.assertEqual(response.code, HTTPStatus.BAD_REQUEST.value)
        self.assertEqual(response.reason, 'Bad Request')

    @mock.patch('brasilprev.views.v1.purchase_item.BaseDefaultFactory')
    def test_method_put_on_known_error(self, mock_base_default_factory):
        mock_base_default_factory.get_instance().process.side_effect = CustomDatabaseError(message='mock error')

        future_1 = Future()
        future_1.set_result({})
        mock_base_default_factory.get_instance().agreement = mock.MagicMock(return_value=future_1)

        response = self.fetch(self.url, raise_error=False, method="PUT", body=json.dumps({}))
        self.assertEqual(response.code, HTTPStatus.BAD_REQUEST.value)
        self.assertEqual(response.reason, 'Bad Request')
