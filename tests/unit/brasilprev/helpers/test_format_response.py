import mock

from brasilprev.helpers.format_response import purchase_response, product_response, customer_response
from tests.unit.brasilprev import BaseTests


class TestFormatResponse(BaseTests):

    def test_purchase_response(self):
        response = purchase_response(response=mock.MagicMock())
        self.assertIsNotNone(response.get('purchase_id'))
        self.assertIsNotNone(response.get('purchase_total_value'))
        self.assertIsNotNone(response.get('customer'))
        self.assertIsNotNone(response.get('product'))
        self.assertIsNotNone(response.get('quantity'))
        self.assertIsNotNone(response.get('total_item_value'))
        self.assertIsNotNone(response.get('purchase_data'))

    def test_product_response(self):
        response = product_response(response=mock.MagicMock())
        self.assertIsNotNone(response.get('id'))
        self.assertIsNotNone(response.get('name'))
        self.assertIsNotNone(response.get('value'))
        self.assertIsNotNone(response.get('quantity'))

    def test_customer_response(self):
        response = customer_response(response=mock.MagicMock())
        self.assertIsNotNone(response.get('id'))
        self.assertIsNotNone(response.get('name'))
        self.assertIsNotNone(response.get('login'))
