import mock

from brasilprev.exceptions import CustomDatabaseError, default_exception_error
from tests.unit.brasilprev import BaseAsyncHttpTestCase


class TestApiPurchaseList(BaseAsyncHttpTestCase):

    @mock.patch('brasilprev.exceptions.logger')
    def test_default_exception_error(self, mock_logger):
        mock_ = mock.MagicMock()
        with self.assertRaises(CustomDatabaseError):
            default_exception_error(model=mock_, error=mock_)
        self.assertTrue(mock_logger.error.called)
