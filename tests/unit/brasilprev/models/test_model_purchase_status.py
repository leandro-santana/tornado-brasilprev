import mock

from brasilprev.models.purchase import PurchaseStatus
from tests.unit.brasilprev import BaseTests


class TestModelPurchaseStatus(BaseTests):

    def test_model_user_get_table_name(self):
        purchase_status = PurchaseStatus()
        response = purchase_status.get_table_name
        self.assertEqual(response, 'purchase_status')

    def test_model_purchase_get_schema_name(self):
        purchase_status = PurchaseStatus()
        response = purchase_status.get_schema_name
        self.assertIsNotNone(response)

    def test_model_purchase_get_purchase_status(self):
        mock_status = {
            "id": 1,
            "name": "pending"
        }
        magic_mock = mock.MagicMock
        purchase_status = PurchaseStatus
        purchase_status().orm.db_session.query = magic_mock(
            return_value=magic_mock(
                filter=magic_mock(return_value=magic_mock(first=magic_mock(return_value=mock_status)))))

        response = purchase_status().get_purchase_status(name='anything')
        self.assertEqual(response, mock_status)
