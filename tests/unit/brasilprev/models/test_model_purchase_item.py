import mock
from tornado.httpclient import HTTPError

from brasilprev.models.purchase import PurchaseItem
from tests.unit.brasilprev import BaseTests


class TestModelPurchaseItem(BaseTests):

    def test_model_customer_get_table_name(self):
        purchase_item = PurchaseItem()
        response = purchase_item.get_table_name
        self.assertEqual(response, 'purchase_item')

    def test_model_customer_get_schema_name(self):
        purchase_item = PurchaseItem()
        response = purchase_item.get_schema_name
        self.assertIsNotNone(response)

    def test_get_product_open(self):
        mock_response = {
            'purchase_item_id': 1,
            'product_id': 1,
            'status': 2
        }
        magic_mock = mock.MagicMock
        purchase_item_model = PurchaseItem
        purchase_item_model().orm.db_session.query = magic_mock(
            return_value=magic_mock(
                join=magic_mock(return_value=magic_mock(
                    filter=magic_mock(return_value=magic_mock(first=magic_mock(return_value=mock_response)))))))

        response = purchase_item_model().get_product_open(product_id=1, purchase_id=1)
        self.assertEqual(response, mock_response)

    def test_get_purchase_item_success(self):
        mock_response = {
            'purchase_id': 1,
            'product_id': 1,
            'quantity': 1,
            'total_item_value': 10.20
        }
        magic_mock = mock.MagicMock
        purchase_item_model = PurchaseItem
        purchase_item_model().orm.db_session.query = magic_mock(
            return_value=magic_mock(filter_by=magic_mock(return_value=mock_response)))
        response = purchase_item_model().get_purchase_item(purchase_item_id=1)
        self.assertEqual(response, mock_response)

    @mock.patch('brasilprev.models.purchase.logger')
    def test_get_purchase_item_error(self, mock_logger):
        magic_mock = mock.MagicMock
        purchase_item_model = PurchaseItem
        purchase_item_model().orm.db_session.query = magic_mock(
            return_value=magic_mock(filter_by=magic_mock(return_value=None)))

        with self.assertRaises(HTTPError) as context:
            purchase_item_model().get_purchase_item(purchase_item_id=1)
        expected_message = 'purchase not found.'
        self.assertEqual(context.exception.message, expected_message)
        self.assertTrue(mock_logger.error.called)

    def test_get_purchase_item_total(self):
        class MockResponse(object):
            total_purchase = 100.00

        magic_mock = mock.MagicMock
        purchase_item_model = PurchaseItem
        purchase_item_model().orm.db_session.query = magic_mock(
            return_value=magic_mock(
                filter=magic_mock(
                    return_value=magic_mock(
                        group_by=magic_mock(return_value=magic_mock(first=magic_mock(return_value=MockResponse())))))))

        response = purchase_item_model().get_purchase_item_total(purchase_id=1)
        self.assertEqual(response, MockResponse.total_purchase)

    def test_create_or_update_purchase_item_product_open(self):
        class MockResponse(object):
            quantity = 10
            total_item_value = 10

        magic_mock = mock.MagicMock
        purchase_item_model = PurchaseItem
        purchase_item_model_instance = purchase_item_model()
        purchase_item_model_instance.orm.object_commit = magic_mock()
        purchase_item_model_instance.get_product_open = magic_mock()
        purchase_item_model_instance.get_purchase_item = magic_mock(
            return_value=magic_mock(
                first=magic_mock(return_value=MockResponse()),
                return_value={}
            )
        )

        response = purchase_item_model_instance.create_or_update_purchase_item(
            product_id=1, purchase_id=1, quantity=1, product_value=10)

        self.assertEqual(response.quantity, 10)
        self.assertEqual(response.total_item_value, 10)

    def test_create_or_update_purchase_item_product_not_open(self):
        magic_mock = mock.MagicMock
        purchase_item_model = PurchaseItem
        purchase_item_model_instance = purchase_item_model()
        purchase_item_model_instance.get_product_open = magic_mock(return_value=None)
        purchase_item_model_instance.orm.object_commit = magic_mock()

        response = purchase_item_model_instance.create_or_update_purchase_item(
            product_id=1, purchase_id=1, quantity=1, product_value=10)
        self.assertEqual(response.quantity, 1)
        self.assertEqual(response.total_item_value, 10)
