from brasilprev.models.user import User
from tests.unit.brasilprev import BaseTests


class TestModelUser(BaseTests):

    def test_model_user_get_table_name(self):
        user = User()
        response = user.get_table_name
        self.assertEqual(response, 'user')

    def test_model_purchase_get_schema_name(self):
        user = User()
        response = user.get_schema_name
        self.assertIsNotNone(response)
