from brasilprev.models.customer import Customer
from tests.unit.brasilprev import BaseTests


class TestModelCustomer(BaseTests):

    def test_model_customer_get_table_name(self):
        customer = Customer()
        response = customer.get_table_name
        self.assertEqual(response, 'customer')

    def test_model_customer_get_schema_name(self):
        customer = Customer()
        response = customer.get_schema_name
        self.assertIsNotNone(response)
