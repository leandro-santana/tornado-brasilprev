import mock
from tornado.httpclient import HTTPError

from brasilprev.models.purchase import Purchase
from tests.unit.brasilprev import BaseTests


class TestModelPurchase(BaseTests):

    def test_model_purchase_get_table_name(self):
        purchase = Purchase()
        response = purchase.get_table_name
        self.assertEqual(response, 'purchase')

    def test_model_purchase_get_schema_name(self):
        purchase = Purchase()
        response = purchase.get_schema_name
        self.assertIsNotNone(response)

    @mock.patch('brasilprev.models.purchase.PurchaseStatus')
    @mock.patch('brasilprev.models.purchase.logger')
    def test_get_or_create_purchase_by_consumer_error(self, mock_logger, mock_purchase_status):
        purchase = Purchase()
        with self.assertRaises(HTTPError) as context:
            purchase.get_or_create_purchase_by_consumer(customer_id=None)
        self.assertTrue(mock_logger.error.called)
        self.assertTrue(mock_purchase_status.called)
        self.assertEqual(context.exception.message, 'customer_id error')

    @mock.patch('brasilprev.models.purchase.config')
    @mock.patch('brasilprev.models.purchase.PurchaseStatus')
    def test_get_or_create_purchase_by_consumer_is_exists(self, mock_purchase_status, mock_config):
        class MockResponse(object):
            total_value = 5.49
            purchase_status_id = 2
            id = 1

        magic_mock = mock.MagicMock
        purchase = Purchase()
        purchase.get_purchase_status = magic_mock(return_value=None)

        purchase.orm.db_session.query = magic_mock(
            return_value=magic_mock(
                filter=magic_mock(
                    return_value=magic_mock(
                        first=magic_mock(return_value=MockResponse())))))

        response = purchase.get_or_create_purchase_by_consumer(customer_id=1)
        self.assertEqual(response.id, 1)
        self.assertEqual(response.total_value, 5.49)
        self.assertEqual(response.purchase_status_id, 2)
        self.assertTrue(mock_purchase_status.called)
        self.assertFalse(mock_config.called)

    @mock.patch('brasilprev.models.purchase.config')
    @mock.patch('brasilprev.models.purchase.PurchaseStatus')
    def test_get_or_create_purchase_by_consumer_not_exists(self, mock_purchase_status, mock_config):
        magic_mock = mock.MagicMock
        purchase = Purchase()
        purchase.get_purchase_status = magic_mock(return_value=None)
        purchase.orm.object_commit = magic_mock()
        purchase.orm.db_session.query = magic_mock(
            return_value=magic_mock(
                filter=magic_mock(
                    return_value=magic_mock(
                        first=magic_mock(return_value=None)))))

        response = purchase.get_or_create_purchase_by_consumer(customer_id=1)

        self.assertTrue(mock_purchase_status.called)
        self.assertFalse(mock_config.called)
        self.assertFalse(response.customer)
        self.assertFalse(response.id)
        self.assertFalse(response.purchase_status)
        self.assertFalse(response.total_value)

    @mock.patch('brasilprev.models.purchase.logger')
    def test_update_purchase_total_value_error(self, mock_logger):
        purchase = Purchase()

        with self.assertRaises(HTTPError) as context:
            purchase.update_purchase_total_value(purchase_id=None, total_purchase=None)

        self.assertEqual(context.exception.message, 'Purchase ID e Total Not Found')
        self.assertTrue(mock_logger.error.called)

    def test_update_purchase_total_value(self):
        magic_mock = mock.MagicMock
        purchase = Purchase()
        purchase.orm.object_commit = magic_mock(return_value=True)
        purchase.orm.db_session.query = magic_mock(
            return_value=magic_mock(
                filter_by=magic_mock())
        )
        response = purchase.update_purchase_total_value(purchase_id=1, total_purchase=1)
        self.assertEqual(response, None)
