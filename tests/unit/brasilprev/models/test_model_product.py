import mock
from tornado.httpclient import HTTPError

from tests.unit.brasilprev import BaseTests

from brasilprev.models.product import Product


class TestModelProduct(BaseTests):

    def setUp(self):
        super(TestModelProduct, self).setUp()
        self.product_model = Product
        self.mock_product = {
            "id": 1,
            "name": "Banana Prata",
            "value": "5.49",
            "quantity": 100
        }

    def test_model_product_get_table_name(self):
        response = self.product_model().get_table_name
        self.assertEqual(response, 'product')

    def test_model_product_get_schema_name(self):
        response = self.product_model().get_schema_name
        self.assertIsNotNone(response)

    def test_get_product_success(self):
        magic_mock = mock.MagicMock
        product_model = self.product_model
        product_model().orm.db_session.query = magic_mock(
            return_value=magic_mock(filter_by=magic_mock(return_value=self.mock_product)))

        response = product_model().get_product(product_id=1)
        self.assertEqual(response, self.mock_product)

    def test_get_product_error(self):
        magic_mock = mock.MagicMock
        product_model = self.product_model
        product_model().orm.db_session.query = magic_mock(
            return_value=magic_mock(filter_by=magic_mock(return_value=None)))

        with self.assertRaises(HTTPError) as context:
            product_model().get_product(product_id=1)
        expected_message = 'The product with code 1 not found'
        self.assertEqual(context.exception.message, expected_message)
