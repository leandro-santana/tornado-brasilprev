import mock
from sqlalchemy.exc import IntegrityError
from tornado.concurrent import Future
from tornado.testing import gen_test

from brasilprev.controller.customer import CustomerController
from brasilprev.models.model_base import ModelBase
from tests.unit.brasilprev import BaseAsyncHttpTestCase


@mock.patch('brasilprev.controller.customer.logger', mock.MagicMock())
class TestCustomerController(BaseAsyncHttpTestCase):

    def setUp(self):
        super(TestCustomerController, self).setUp()
        self.customer_controller = CustomerController()
        self.expected_response = {}

    @gen_test
    async def test_process(self):
        message = 'anything'
        future_1 = Future()
        future_1.set_result(message)

        self.customer_controller._run_process = mock.MagicMock(return_value=future_1)
        response = await self.customer_controller.process(params={})

        self.assertEqual(message, response)

    @mock.patch('brasilprev.helpers.validator.User', mock.MagicMock())
    @mock.patch('brasilprev.controller.customer.validate_default_with_min_value', mock.MagicMock())
    @mock.patch('brasilprev.controller.customer.parser')
    @gen_test
    async def test_agreement(self, mock_parser):
        expected_response = {
            "name": "anything",
            "value": 10,
            "quantity": 10
        }

        mock_parser.parse.return_value = expected_response
        response = await self.customer_controller.agreement(request=mock.MagicMock())

        self.assertEqual(expected_response, response)

    @gen_test
    async def test_method_post_success(self):
        self.customer_controller.model = mock.MagicMock()

        response = await self.customer_controller.method_post(id=1)
        self.assertEqual('object created successfully', response)

    @mock.patch('brasilprev.controller.customer.default_exception_error')
    @gen_test
    async def test_method_post_error(self, mock_default_exception_error):
        self.customer_controller.model = mock.MagicMock(
            KNOWN_ERROR_SQLALCHEMY=ModelBase().KNOWN_ERROR_SQLALCHEMY,
            orm=mock.MagicMock(object_commit=mock.MagicMock(side_effect=IntegrityError('mock', 'mock', 'mock')))
        )
        await self.customer_controller.method_post(id=1)
        self.assertTrue(mock_default_exception_error.called)
