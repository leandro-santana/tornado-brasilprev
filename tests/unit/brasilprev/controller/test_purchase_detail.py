import mock
from sqlalchemy.exc import IntegrityError
from tornado.httpclient import HTTPClientError
from tornado.testing import gen_test
from tornado.concurrent import Future

from brasilprev.controller.purchase_detail import PurchaseDetail
from brasilprev.models.model_base import ModelBase
from tests.unit.brasilprev import BaseAsyncHttpTestCase


@mock.patch('brasilprev.controller.purchase_detail.logger', mock.MagicMock())
class TestPurchaseDetail(BaseAsyncHttpTestCase):
    def setUp(self):
        super(TestPurchaseDetail, self).setUp()
        self.purchase_detail = PurchaseDetail()
        self.mock_response = {
            "result": {
                "resultsCount": 1,
                "next_page": None,
                "previous_page": None,
                "data": [{}]
            },
            "status": "success"
        }

    @gen_test
    async def test_process(self):
        future_1 = Future()
        future_1.set_result(self.mock_response)
        self.purchase_detail._run_process = mock.MagicMock(return_value=future_1)
        response = await self.purchase_detail.process(params={})
        self.assertEqual(response, self.mock_response)

    @mock.patch('brasilprev.controller.purchase_detail.purchase_response')
    @gen_test
    async def test_result_mount(self, mock_purchase_response):
        await self.purchase_detail.result_mount(mock.MagicMock())
        self.assertTrue(mock_purchase_response.called)

    @mock.patch('brasilprev.helpers.validator.Customer', mock.MagicMock())
    @mock.patch('brasilprev.controller.purchase_detail.parser')
    @gen_test
    async def test_agreement(self, mock_parser):
        expected_response = {
            "name": "anything",
            "value": 10,
            "quantity": 10
        }

        mock_parser.parse.return_value = expected_response
        response = await self.purchase_detail.agreement(request=mock.MagicMock())

        self.assertIsNotNone(response.get('name'))
        self.assertIsNotNone(response.get('value'))
        self.assertIsNotNone(response.get('quantity'))
        self.assertIsNotNone(response.get('customer_id'))

    @mock.patch('brasilprev.controller.purchase_detail.PurchaseItem')
    @mock.patch('brasilprev.controller.purchase_detail.paginate')
    @gen_test
    async def test_query_mount_success(self, mock_paginate, mock_customer):
        await self.purchase_detail.query_mount('purchase_id', 1, 0, 1)
        self.assertTrue(mock_paginate.called)
        self.assertTrue(mock_customer.called)

    @mock.patch('brasilprev.controller.purchase_detail.PurchaseItem')
    @mock.patch('brasilprev.controller.purchase_detail.paginate')
    @mock.patch('brasilprev.controller.purchase_detail.default_exception_error')
    @gen_test
    async def test_query_mount_error(self, mock_default_exception_error, mock_paginate, mock_model):
        mock_model().KNOWN_ERROR_SQLALCHEMY = ModelBase().KNOWN_ERROR_SQLALCHEMY
        mock_paginate.side_effect = IntegrityError('mock', 'mock', 'mock')

        await self.purchase_detail.query_mount('purchase_id', 1, 0, 1)

        self.assertTrue(mock_paginate.called)
        self.assertTrue(mock_model.called)
        self.assertTrue(mock_default_exception_error.called)

    @mock.patch('brasilprev.controller.purchase_detail.PurchaseItem')
    @gen_test
    async def test_query_mount_exception(self, mock_product):
        with self.assertRaises(HTTPClientError) as context:
            await self.purchase_detail.query_mount('unknown_name', 'a', 0, 1)
        self.assertTrue(mock_product.called)
        self.assertEqual(context.exception.message, 'target "unknown_name" not found in model Purchase')
