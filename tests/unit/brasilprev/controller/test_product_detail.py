import mock
from sqlalchemy.exc import IntegrityError
from tornado.httpclient import HTTPClientError
from tornado.testing import gen_test
from tornado.concurrent import Future

from brasilprev.controller.product_detail import ProductDetail
from brasilprev.models.model_base import ModelBase
from tests.unit.brasilprev import BaseAsyncHttpTestCase


@mock.patch('brasilprev.controller.product_detail.logger', mock.MagicMock())
class TestProductDetail(BaseAsyncHttpTestCase):
    def setUp(self):
        super(TestProductDetail, self).setUp()
        self.product_detail = ProductDetail()
        self.mock_response = {
            "result": {
                "resultsCount": 1,
                "next_page": None,
                "previous_page": None,
                "data": [{}]
            },
            "status": "success"
        }

    @gen_test
    async def test_process(self):
        future_1 = Future()
        future_1.set_result(self.mock_response)
        self.product_detail._run_process = mock.MagicMock(return_value=future_1)
        response = await self.product_detail.process(params={})
        self.assertEqual(response, self.mock_response)

    @mock.patch('brasilprev.controller.product_detail.product_response')
    @gen_test
    async def test_result_mount(self, mock_product_response):
        await self.product_detail.result_mount(mock.MagicMock())
        self.assertTrue(mock_product_response.called)

    @mock.patch('brasilprev.helpers.validator.User', mock.MagicMock())
    @mock.patch('brasilprev.controller.product_detail.default_list_agreement_agreement')
    @gen_test
    async def test_agreement(self, mock_list_agreement_default):
        expected_response = {
            "name": "anything",
            "value": 10,
            "quantity": 10
        }

        mock_list_agreement_default.return_value = expected_response
        response = await self.product_detail.agreement(request=mock.MagicMock())

        self.assertEqual(expected_response, response)

    @mock.patch('brasilprev.controller.product_detail.Product')
    @mock.patch('brasilprev.controller.product_detail.paginate')
    @gen_test
    async def test_query_mount_success(self, mock_paginate, mock_product):
        await self.product_detail.query_mount('id', 1, 0, 1)
        self.assertTrue(mock_paginate.called)
        self.assertTrue(mock_product.called)

    @mock.patch('brasilprev.controller.product_detail.Product')
    @mock.patch('brasilprev.controller.product_detail.paginate')
    @mock.patch('brasilprev.controller.product_detail.default_exception_error')
    @gen_test
    async def test_query_mount_error(self, mock_default_exception_error, mock_paginate, mock_model):
        mock_model().KNOWN_ERROR_SQLALCHEMY = ModelBase().KNOWN_ERROR_SQLALCHEMY
        mock_paginate.side_effect = IntegrityError('mock', 'mock', 'mock')

        await self.product_detail.query_mount('id', 1, 0, 1)

        self.assertTrue(mock_paginate.called)
        self.assertTrue(mock_model.called)
        self.assertTrue(mock_default_exception_error.called)

    @mock.patch('brasilprev.controller.product_detail.Product')
    @gen_test
    async def test_query_mount_exception(self, mock_product):
        with self.assertRaises(HTTPClientError) as context:
            await self.product_detail.query_mount('unknown_name', 'a', 0, 1)
        self.assertTrue(mock_product.called)
        self.assertEqual(context.exception.message, 'target "unknown_name" not found in model Product')
