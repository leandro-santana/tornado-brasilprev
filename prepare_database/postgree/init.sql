create user usr_dev with PASSWORD '123456';

create DATABASE brasilprev
    with
    OWNER = usr_dev
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

grant all privileges on DATABASE brasilprev to usr_dev;

\connect brasilprev;

create SCHEMA brasilprev
    AUTHORIZATION usr_dev;

SET search_path TO brasilprev;

ALTER USER usr_dev CREATEDB;
